import {
  dateFormat,
  currency
} from './tools.js'

/**
 * 日期格式化拓展函数  new Date().format("YYYY-MM-DD") 返回2019-04-03
 * new Date().format("YYYY-MM-DD hh:mm:ss") 返回2019-04-03 09:20:10
 */

const expandObj = () => {
  //时间日期格式化
  Number.prototype.format =
    String.prototype.format =
    Date.prototype.format =
      function (fmt = 'YYYY-MM-DD', lunarDay) {
        let date = null
        try {
          date = new Date(this)
          return dateFormat(date, fmt, lunarDay)
        } catch (e) {
          return '-'
        }
      }
  // 数字货币化
  Number.prototype.currency = String.prototype.currency = function (precision = 2) {
    const def = 0
    if (!this) return def.toFixed(precision)
    let num = parseFloat(this)
    return currency(num, precision)
  }
}
export default expandObj
