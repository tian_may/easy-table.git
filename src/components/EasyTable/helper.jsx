import _ from 'lodash'

// 必填项
export const requiredRule = [
  {
    required: true,
    message: '必填',
    trigger: ['blur', 'change']
  }
]
//分页默认配置
export const pageInit = {
  pageNum: 1,
  pageSize: 10,
  total: 1
}

// 搜索配置项
export const getSearchConfig = (columns) => {
  const res = columns
    ?.filter(
      (item) => !item?.hideInSearch && item.valueType !== 'operate' && item.valueType !== 'index'
    )
    ?.map((el, index) => ({
      ...el,
      prop: el?.field || el.prop,
      span: el?.span || 6,
      index: el?.index || index
    }))
    ?.sort((a, b) => {
      return (a?.index || 0) - (b?.index || 0)
    })
  return res || []
}
// 保存原值
export const addOldVal = (row) => {
  const others = _.omit(row, ['oldVal']);
  row.oldVal = JSON.stringify(others || {});
};
// 初始化双向绑定数据
export const initItem = (row = {}, columns = [], multiple) => {
  columns.forEach((item) => {
    const {prop, initValue} = item;
    if (!item.hideInTable && prop) {
      row[prop] = row[prop] || initValue || (multiple ? undefined : '-');
      //多行编辑  默认可编辑 edit=true
      if (multiple) {
        row.edit = true
      } else {
        //单行可编辑 默认添加oldVal
        addOldVal(row);
        row.edit = false
      }
    }
  });
  return row;
};
// 处理Columns
export const getEditColumns = (columns, disabled) => columns
  ?.filter((item) => !item?.hideInTable && !(disabled && (item.valueType === 'operate')))
  ?.map((item) => {
    return {
      fixed: item.valueType === 'operate' && 'right',
      edit: ({row}) => row?.edit,
      ...item,
    };
  });
