import Select from "@/components/XTable/components/FormItem/XSelect.vue";
import AreaSelect from "@/components/XTable/components/FormItem/AreaSelect.vue";
import {ElInput, ElInputNumber, ElDatePicker} from 'element-plus';

export const enumFormType = {
  INPUT: 'input',
  AREASELECT: 'areaselect',
  SELECT: 'select',
  UPLOAD: 'upload',
  NUMBER: 'number',
  CHECKBOX: 'checkbox',
  TEXTAREA: 'textarea',
  DATETIME: 'datetime',
  DATETIMEMINUTE: 'datetimeminute',
  DATERANGE: 'daterange',
  DATETIMERANGE: 'datetimerange',
  DATETIMERANGEMINUTE: 'datetimerangeminute',
  SWITCH: 'switch',
  RADIO: 'radio',
  SELECTLANG: 'selectlang',
  DATE: 'date',
  SELECTLABELINPUT: 'selectlabelinput',
  SELECTTAG: 'selecttag',
  SELECTCOUNTRYREGION: 'selectcountryregion',
  DATERANGESELECT: 'daterangeselect',
  SELECTPLATFORM: 'selectplatform',
  NUMRANGESELECT: 'numrangeselect',
}

export const enumFormItem = {
  [enumFormType.INPUT]: {
    name: ElInput,
    props: {
      style: 'width: 100%',
      clearable: true,
      placeholder: '请输入',
    },
  },
  [enumFormType.TEXTAREA]: {
    name: ElInput,
    props: {
      style: 'width: 100%',
      clearable: true,
      placeholder: '请输入',
      type: 'textarea',
      rows: 2
    },
  },
  [enumFormType.SELECT]: {
    name: Select,
    props: {
      style: 'width: 100%',
      clearable: true,
      placeholder: '请选择',
    },
  },
  [enumFormType.AREASELECT]: {
    name: AreaSelect,
    props: {
      style: 'width: 100%',
      clearable: true,
      placeholder: '请选择',
    },
  },
  [enumFormType.NUMBER]: {
    name: ElInputNumber,
    props: {
      class: 'text-left',
      style: 'width: 100%',
      placeholder: '请输入',
      controls: false,
      clearable: true,
    },
  },
  [enumFormType.DATE]: {
    name: ElDatePicker,
    props: {
      type: 'date',
      style: 'width: 100%',
      placeholder: '请选择',
    },
  },
  [enumFormType.DATETIME]: {
    name: ElDatePicker,
    props: {
      type: 'datetime',
      style: 'width: 100%',
      placeholder: '请选择',
    },
  },
  [enumFormType.DATERANGE]: {
    name: ElDatePicker,
    props: {
      type: 'daterange',
      style: 'width: 100%',
    },
  },
  [enumFormType.DATETIMERANGE]: {
    name: ElDatePicker,
    props: {
      style: 'width: 100%',
      type: 'datetimequickrange',
      defaultTime: ['00:00:00', '23:59:59'],
    },
  },
}

