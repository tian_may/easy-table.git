import AreaSelect from './components/AreaSelect.vue'
import Select from './components/Select.vue'

// 必填项
export const requiredRule = [{required: true, message: '必填', trigger: ['blur', 'change']}]
//分页默认配置
export const pageInit = {
  currentPage: 1,
  pageSize: 10,
  total: 1
}

/**splitArr2Arr以list中的key为属性值 将key值相同的对象归类到key中
 * @param {Array} list 要分割的数组
 * @param {Object} key 字段名
 */
export function splitArr2Arr(list, key) {
  if (!list.length) return []
  if (!key) return list
  let mySet = new Set()
  list.forEach((item) => {
    mySet.add(item[key])
  })
  return groupArr2Arr(list, [...mySet], key)
}

/**groupArr2Arr将list按照group中的key值进行分组
 * @param {Array} list 要分割的数组
 * @param {Object} group 要分割的规则数组
 * @param {Object} key 字段名
 */
export function groupArr2Arr(list, group, key) {
  let obj = {}
  group.forEach((i) => {
    obj[i] = []
    list.forEach((item) => {
      if (item[key] === i) {
        obj[i].push(item)
      }
    })
  })
  let arr = []
  Object.keys(obj).forEach((item) => {
    arr.push({
      [key]: item,
      list: obj[item]
    })
  })
  return arr
}

//表格props配置
export const props = (edit = false) => ({
  // 表格列配置项
  columns: {
    type: Array,
    default: () => []
  },
  // 表格数据
  dataSource: {
    type: Array,
    default: null
  },
  // 分页配置
  pagination: {
    type: [Object, Boolean],
    default: edit ? false : pageInit
  },
  // 表格标题
  headerTitle: {
    type: String,
    default: ''
  },
  // 是否显示搜索表单
  search: {
    type: Boolean,
    default: !edit
  },
  // 搜索表单labelWidth
  labelWidth: {
    type: [Number, String],
    default: 130
  },
  // 是否手动请求接口，为true则不会初始化request请求
  manualRequest: {
    type: Boolean,
    default: false
  },
  // 是否可选择行
  rowSelection: {
    type: Object,
    default: null
  },
  // 表格合计
  summary: {
    type: Object,
    default: null
  },
  // 请求
  request: {
    type: Function,
    default: null
  },
  // 默认选中行
  defaultSelection: {
    type: Array,
    default: null
  },
  // 扩展请求参数，可以作为初始化查询条件值
  params: {
    type: Object,
    default: null
  },
  // 表格数据
  form: {
    type: Object,
    default: () => []
  },
  // 可编辑表格的配置
  editable: {
    type: Object,
    default: null
  },
  // 是否可以删除最后一行
  canDelLast: {
    type: Boolean,
    default: false
  },
  // 表格配置项，默认为false不展示,可以设置为true展示，也可以传需要展示的配置项数组，如：['setting,'reload']
  options: {
    type: [Boolean, Array],
    default: false
  }
})

// 单元格合并
export function getMerge(mergeCol, data) {
  if (!data) return []
  const res = data
  mergeCol?.map?.((obj) => {
    const groups = splitArr2Arr(data, obj.prop)
    const res = []
    groups?.forEach?.((arr) => {
      arr?.list?.forEach?.((item, index) => {
        item[`rowSpan_${obj.prop}`] = index === 0 ? arr?.list?.length : 0
      })
      res?.concat?.(arr?.list)
    })
  })
  return res || []
}

// 数字千分位
export const currency = function (num = 0, precision = 2, separator = ',') {
  var parts
  if (!isNaN(parseFloat(num)) && isFinite(num)) {
    num = Number(num)
    num = num.toFixed(precision).toString()
    parts = num.split('.')
    parts[0] = parts[0].toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, `$1${separator}`)

    return parts.join('.')
  }
  return num
}
// 获取可合并的列
export const getMergeCol = (columns, rowSelection) => {
  return columns?.filter?.((el, index) => {
    if (el.merge === 'col') {
      return el
    }
    //当表格为可选表格时 chekcbox/radio所在列展示 合计
    if (index === 1 && columns[1].merge === 'col' && rowSelection) {
      return el
    }
  })
}
//获取要显示的列
export const getColumns = (columns) => {
  return columns
    ?.filter((item) => !item?.hideInTable && !item?.hideInOptions)
    ?.map((item) => {
      const {valueType, edit = true} = item
      if (valueType === 'action') {
        item.width = item.width || 130
        item.align = item.align || 'center'
        item.fixed = item.fixed || 'right'
      } else if (columns.length > 10) {
        const str = item?.label?.replace?.(/\(|\（|\）|\)/g, '')
        item.width = item.width || (edit ? 150 : (str?.length || 2) * 20 + 30)
      }
      return item
    })
}

// 单元格合并
export const objectSpanMethodFun = ({row, column}, mergeCol) => {
  const props = mergeCol?.map((el) => el.prop)
  if (props.includes(column.property)) {
    const rowspan = row[`rowSpan_${column.property}`]
    const res = [rowspan, rowspan > 0 ? 1 : 0]
    return res
  }
}
// 汇总默认方法
export const summaryMethodFun = (param = {}, column = [], sumText = '汇总：') => {
  const summaryObj = {}
  const summaryArr = column?.map((el, index) => {
    const {summary} = el
    const {prop, precision = 0} = summary || {}
    if (index === 0) {
      return sumText
    } else {
      if (!summary) return
      const sum =
        param?.data?.reduce((total, cur) => {
          return Number(cur[prop || el?.prop] || 0) + total
        }, 0) || 0
      const num = sum.toFixed(precision)
      summaryObj[el.prop] = num
      return num
    }
  })
  return {summaryArr, summaryObj}
}
// 搜索配置项
export const getSearchConfig = (columns) => {
  const res = columns
    ?.filter(
      (item) => !item?.hideInSearch && item.valueType !== 'action' && item.valueType !== 'index'
    )
    ?.map((el, index) => ({
      ...el,
      prop: el?.field || el.prop,
      index: el?.index || index
    }))
    ?.sort((a, b) => {
      return (a?.index || 0) - (b?.index || 0)
    })
  return res || []
}
const select = (item, row, $index, attrs = {}) => {
  const {query, valueEnum, prop} = item
  const text = row?.[prop]
  const items = {
    ...item,
    ...attrs,
    query: typeof query === 'function' ? query(text, row, $index) : query,
    valueEnum: typeof valueEnum === 'function' ? valueEnum(text, row, $index) : valueEnum,
    remoteMethod: item?.remoteMethod ? query => item?.remoteMethod?.(query, row, $index) : null,
  }
  return <Select {...items} v-model:value={row[prop]}/>
}
// 显示表格单元格
export const getItem = (item = {}, scope) => {
  const {$index, row} = scope
  const {prop} = item
  const text = row?.[prop]
  const res = row[prop] || row[prop] === 0 ? row[prop] : '-'
  return {
    input: () => res,//普通值
    index: () => $index,//索引
    date: () => (row?.[prop] || 0)?.format?.(),//日期格式
    time: () => (row?.[prop] || 0)?.format?.('YYYY-MM-DD hh:mm:ss'), //时间格式
    //数值类型 为空时展示0（而非-） 小数位控制（默认0）
    number: () => {
      const value = row[prop] || 0
      const precision = item?.precision || item?.summary?.precision || 0
      return Number(value).toFixed(precision).toString()
    },
    //超出width宽度... 悬浮toolTip提示
    ellipsis: () => text ? (
      <el-tooltip content={text} placement="top">
        <p class="truncate">{res}</p>
      </el-tooltip>
    ) : '-',
    //超出width宽度3行... 悬浮toolTip提示
    textarea: () => text ? (
      <el-tooltip content={res} placement="top">
        <p class="overflow-ellipsis overflow-hidden line-clamp-3">{res}</p>
      </el-tooltip>
    ) : '-',
    //枚举值
    select: () => select(item, row, $index, {
      placeholder: '-',
      disabled: true,
      class: 'is-readonly'
    }),
    areaSelect: () => text ? <p class="w-full">
      <AreaSelect {...item} v-model:value={row[prop]} class="is-readonly" disabled placeholder="-"/>
      <p>{row[item?.address]}</p>
    </p> : '-',
    //货币格式（为空时展示0（而非-） 小数位控制（默认2）
    currency: () => currency((+row[prop] || 0).toFixed(item.precision || 2)).toString()
  }
}

const valueTypeMap = (item, {row = {}, $index}) => {
  if (!item) return null
  const {prop} = item
  const text = row?.[prop]
  return {
    number: () => {
      const {min, max} = item
      const items = {
        ...item,
        max: typeof max === 'function' ? max?.(text, row, $index) : max,
        min: typeof min === 'function' ? min?.(text, row, $index) : min,
      }
      return <el-input-number {...items} controls={false} v-model={row[prop]}/>
    },
    select: () => select(item, row, $index),
    date: () => <el-date-picker
      format='YYYY-MM-DD'
      value-format='x'
      {...item}
      type="date"
      v-model={row[prop]}
      placeholder="请选择"
      start-placeholder="起始日期"
      end-placeholder="截止日期"
    />,
    time: () => <el-time-picker
      format='HH:mm:ss'
      value-format='x'
      {...item}
      v-model={row[prop]}
      style={`width:${item.width - 10}px`}
      placeholder="请选择"
      start-placeholder="开始时间"
      end-placeholder="结束时间"
    />,
    textarea: () => <el-input
      maxlength={100}
      autosize={{minRows: 3, maxRows: 4}}
      {...item}
      v-model={row[prop]}
      clearable
      show-word-limit
      type="textarea"
    />,
    numberString: () => <el-input
      maxlength={30}
      {...item}
      v-string-number
      οninput={value => (value = value.replace(/[^0-9.]/g, ''))}
      v-model={row[prop]}
    />,
    areaSelect: () => <p>
      <AreaSelect
        {...item}
        v-model:value={row[prop]}/>
      {item?.address && (
        <el-form-item
          label-width="0"
          class="mt-1"
          prop={item.prop ? `data.${$index}.${item?.address}` : null}
          rules={item?.required && requiredRule}>
          <el-input
            type="text"
            {...item}
            v-model={row[item?.address]}
            placeholder="详细地址"/>
        </el-form-item>
      )}
    </p>,
    switch: () => <el-switch {...item} v-model={row[prop]}/>,
    input: () => <el-input maxlength={30} {...item} v-model={row[prop]}/>
  }
}
// 表单类型
export const itemValueType = (type = 'input', item = {}, scope = {}, valueTypeMaps, isEdit = true) => {
  if (!item) return null
  const {$index, row = {}} = scope
  let {disabled, defaultValue} = item
  const text = row[item?.prop]
  //初始化值
  const initValue = typeof defaultValue === 'function' ? defaultValue(text, row, $index) : defaultValue
  if (initValue) {
    row[item?.prop] = row[item?.prop] || initValue
  }
  const onChange = (val) => {
    item?.onChange?.(val, row, $index)
  }
  const prop = {
    ...item,
    onChange,
    disabled: typeof disabled === 'function' ? disabled(item, row, $index) : disabled,
    readonly: !isEdit,
  }
  const otherMaps = typeof valueTypeMaps === "function" ? valueTypeMaps?.(prop, scope) : {}
  const res = {...valueTypeMap?.(prop, scope), ...otherMaps}
  return res?.[type]?.()
}
