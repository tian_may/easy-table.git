import {computed} from 'vue'
import {enumFormItem, enumFormType} from "@/components/XTable/components/FormItem/index.js";
import {safeParseJSON} from "@/utils/tools.js";
import _ from 'lodash'
import {requiredRule, addOldVal} from "@/components/XTable/helper.jsx";


const useEditTableCol = ({props, formRef, columns},formData) => {
  const multiple = computed(() => props?.value?.multiple)
// header插槽
  const headerSlot = (scope, item) => {
    const props = getProps(scope, item);
    const {header, tip} = props;
    const labelEl = (
      <span
        class={{
          'is-required': props.required,
        }}
      >{header || props.label}</span>
    );
    if (tip) {
      const content = _.isFunction(tip.content) ? tip.content?.() : tip.content;
      const type = ['info', 'question'].includes(tip.type) ? tip.type : 'question';
      return (
        <div class="flex items-center">
          {labelEl}
          <mi-tooltip placement="top" effect="light">
            <div slot="content">{content}</div>
            <i class={`mi-icon-${type} ml-2`}/>
          </mi-tooltip>
        </div>
      );
    }
    return labelEl;
  };

  const getProps = (scope, item, formData) => {
    const obj = {};
    Object.entries(item)
      .forEach(([key, value]) => {
        obj[key] = (typeof value === 'function') ? value?.(scope, formData) : value;
      });
    return obj;
  };

// 删除行
  const removeRow = async (item, {$index}) => {
    const res = await item.on?.remove?.();
    if (item.on?.remove && !res) {
      return false;// 如果 onRemove 返回false则禁止保存
    }
    return formData?.value?.splice($index, 1);
  };
// 取消编辑
  const onCancel = (item, {row, $index}) => {
    if (!row?.oldVal) {
      removeRow(item, {row, $index});
    }
    const res = safeParseJSON(row?.oldVal, {});
    columns.value?.forEach((el) => {
      const key = el.field || el.prop;
      if (key && key !== 'oldVal') {
        row[key] = res?.[key];
      }
    });
    // 取消表单校验
    formRef?.value?.clearValidate?.();
    row.edit = false;
  };

// 编辑
  const onEdit = (item, {$index: index}) => {
    formData.value?.forEach((row, $index) => {
      if ($index === index) {
        addOldVal(row);
        row.edit = true;
      } else {
        onCancel(item, {row, $index});
      }
    });
  };
// 保存行
  const onSave = (item, {row}) => {
    formRef?.value?.validate?.(async (valid, invalidFields) => {
      console.log(valid);
      if (valid) {
        // 当前配置项提供 onSave 时，调用它并传递当前行数据
        const res = await item.on?.save?.();
        // 如果 onSave 返回false则禁止保存
        if (item.on?.save && !res) {
          return false;
        }
        addOldVal(row);
        row.edit = false;
      } else {
        return false;
      }
      return true;
    });
  };
// 操作类型的col
  const actionRender = (type, scope, item) => {
    const {row} = scope
    switch (type) {
      case 'remove':
        return (!row?.edit || multiple.value) ? (
          <el-popconfirm
            title="确认删除？"
            onConfirm={() => removeRow(item, scope)}
          >
            {{
              reference: () => (
                <el-link
                  type="danger"
                  underline={false}
                  class="mr-2"
                >
                  删除
                </el-link>
              )
            }}
          </el-popconfirm>
        ) : null
      case 'edit':
        if (multiple.value) return null
        return row?.edit ? (
          [
            <el-link
              class="mr-2"
              underline={false}
              type="primary"
              onClick={() => onSave(item, scope)}
            >
              保存
            </el-link>,
            <el-link
              class="mr-2"
              underline={false}
              type="info"
              onClick={() => onCancel(item, scope)}
            >
              取消
            </el-link>
          ]
        ) : (
          <el-link
            class="mr-2"
            underline={false}
            type="primary"
            onClick={() => onEdit(item, scope)}
          >
            编辑
          </el-link>
        )
      // 其他类型 如：Element 直接渲染
      default:
        return type
    }
  };

// 表格项
  const formItem = (scope, item = {}) => {
    if (!item) return null;
    const {
      prop,
      render,
      edit = true,
      disabled,
      valueType = enumFormType.INPUT,
    } = item;
    const action = item.props?.action || (!multiple.value ? ['edit', 'remove'] : ['remove'])
    // render类型
    if (_.has(item, 'render')) return render;// 有自定义渲染函数
    //操作类型
    if (valueType === 'operate') {
      return action.map?.((it) => actionRender(it, scope, item, props, formData)) || '-';
    }
    // type 类型
    const comp = enumFormItem[valueType];
    const config = {
      modelValue: scope.row[prop],
      'onUpdate:modelValue': (value) => scope.row[prop] = value,
      directives: item.directives,
      on: item.listeners,
      ...comp?.props, // 默认的props
      ...item.props,
      disabled: !edit || props.value?.disabled || disabled,
    };
    return comp && h(comp.name, config);
  };

// 默认插槽
  const defaultSlot = (scope, item = {}, formData) => {
    const props = getProps(scope, item, formData);
    const prop = props.prop ? `data.${scope.$index}.${props.prop}` : null;
    const rules = props.rules || (props?.required ? requiredRule : null);
    return (
      <mi-form-item
        {...{...item, prop, rules}}
        key={prop}
        class={{'is-readonly': !props?.edit}}
      >
        {formItem(scope, props)}
      </mi-form-item>
    );
  };
  const tableColRender = () => {
    return columns.value?.map((item) => {
      const slot = {
        header: (scope) => headerSlot(scope, item),
        default: (scope) => defaultSlot(scope, item, props, formData),
      }
      return (
        <el-table-column {...item} key={item.prop}>
          {slot}
        </el-table-column>
      )
    })
  }
  return {
    tableColRender,
    onCancel
  }
}
export default useEditTableCol
