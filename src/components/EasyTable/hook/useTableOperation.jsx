import {RefreshRight} from '@element-plus/icons-vue'
import Dropdown from '../components/Dropdown.vue'
import {ref} from 'vue'
import useTableCol from "@/components/XTable/hook/useTableCol.jsx";

const useTableOperation = ({options, columns, reload}) => {
  const showColumns = ref(columns.value)
  watch(() => columns.value,
    (value) => {
      showColumns.value = value
    },
    {
      deep: true,
      immediate: true,
    })
  const {tableColRender} = useTableCol(showColumns)
  //表格设置
  const option = [
    {
      content: <RefreshRight onClick={reload}/>,
      tip: '刷新',
      id: 'reload'
    },
    {
      content: <Dropdown
        v-model:columns={showColumns.value}
        onSave={options?.value?.onSave}
      />,
      tip: '列设置',
      id: 'setting'
    }
  ]?.filter?.((el) => {
    const arr = options?.value?.showOptions
    if (Array.isArray(arr)) {
      return arr.includes(el.id)
    } else {
      return el.id
    }
  })
  //表格渲染
  const tableOperationRender = () => {
    return options?.value && (
      <div class="flex justify-end items-center ml-2" style="line-height:0">
        {option?.map((el) => (
          <div class="mr-4">
            <el-tooltip
              effect="dark"
              content={el.tip}
              placement="top"
              class="box-item"
            >
              <el-icon size={18} color="#666" class="cursor-pointer">
                {el.content}
              </el-icon>
            </el-tooltip>
          </div>
        ))}
      </div>
    )
  }
  return {
    tableOperationRender,
    tableColRender
  }
}
export default useTableOperation
