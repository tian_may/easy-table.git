// 汇总默认方法
export const summaryMethodFun = (param = {}, column = []) => {
  const summaryObj = {}
  const summaryArr = column?.map((el, index) => {
    const {summary} = el
    const {
      prop,
      precision = 0,
      text = '汇总'
    } = summary || {}
    if (index === 0) {
      return text
    } else {
      if (!summary) return
      const sum =
        param?.data?.reduce((total, cur) => {
          return Number(cur[prop || el?.prop] || 0) + total
        }, 0) || 0
      const num = sum.toFixed(precision)
      summaryObj[el.prop] = num
      return num
    }
  })
  return {summaryArr, summaryObj}
}
const useSummary = ({columns, rowSelection, summary, slots}) => {
  const summaryMethod = (param) => {
    const {summaryArr, summaryObj} = summaryMethodFun(param, columns?.value) || {}
    summary?.value?.getSummary?.(summaryObj)
    //可选择表格 去掉第一个占位符
    if (rowSelection?.value) {
      summaryArr.splice(1, 0, '')
    }
    //嵌套可展开表格 加上第一个占位符
    if (slots?.expand?.()) {
      summaryArr.unshift('')
    }
    return summaryArr
  }
  return {
    summaryMethod
  }
}
export default useSummary

