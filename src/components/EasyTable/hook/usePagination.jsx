import {ref} from 'vue'

const PAGINATION_CONFIG = {
  pageSizes: [10, 15, 20, 30, 50],
  layout: 'total, sizes, prev, pager, next, jumper',
}
const pageInit = {
  pageNum: 1,
  pageSize: 10,
  total: 1
}
const pageChange = (onChange) => ({
  onCurrentChange: onChange,
  onSizeChange: onChange,
  onPrevClick: onChange,
  onNextClick: onChange
})

const usePagination = (pagination) => {
  const pageParam = ref({...pageInit})
  const paginationRender = () => {
    return (
      <div class="flex justify-end mt-4">
        <el-pagination
          v-model:current-page={pageParam.value.pageNum}
          v-model:page-size={pageParam.value.pageSize}
          v-model:total={pageParam.value.total}
          {...{
            ...PAGINATION_CONFIG,
            ...pagination.value,
            ...pageChange(pagination.onChange)
          }}
        />
      </div>)
  }
  return {
    paginationRender,
    pageParam
  }
}
export default usePagination
