import {dateFormat, numberFormat} from "@/utils/tools";
import {useRouter} from 'vue-router'
import {computed} from 'vue'
import {enumFormType} from "@/components/XTable/components/FormItem/index.js";
//获取要显示的列
export const getColumns = (columns) => {
  return columns
    ?.filter((item) => !item?.hideInTable && !item?.hideInOptions)
    ?.map((item) => {
      const {valueType, edit = true} = item
      if (valueType === 'operate') {
        item.width = item.width || 130
        item.align = item.align || 'center'
        item.fixed = item.fixed || 'right'
      } else if (columns.length > 10) {
        const str = item?.label?.replace?.(/\(|\（|\）|\)/g, '')
        item.width = item.width || (edit ? 150 : (str?.length || 2) * 20 + 30)
      }
      return item
    })
}

const router = useRouter()
const GO_METHOD = {
  resolve: 'resolve',
  push: 'push',
  replace: 'replace',
};

// proTable的render渲染类型
const PROTABLE_RENDER_TYPE = {
  ...enumFormType,
  OPERATE: 'operate',
  TAG: 'tag',
};

export function isFunction(fn) {
  return fn && typeof fn === 'function'
}

function isTruthyOrZero(value) {
  return value || value === 0;
}

// actionList跳转
const handleLink = (config = {}) => {
  if (config.onClick) return config.onClick()
  const goMethod = config.method || GO_METHOD.resolve
  const route = router?.[goMethod]({
    name: config.name,
    query: config.query,
  })
  if (goMethod === GO_METHOD.resolve) {
    window.open(route?.href, '_blank') // 打开新的页面
  }
}
const getActionList = (list = []) => {
  if (!Array.isArray(list)) {
    return null;
  }
  const filteredList = list.filter((item) => !item.hide);
  if (!filteredList.length) {
    return null;
  }
  const config = {
    size: 'small',
    underline: false,
  };
  return filteredList?.map((item) => {
    const itemConf = {...config, ...(item.config || {})}
    return item.popconfirm ?
      (<el-popconfirm
        title={`确认${item?.name}？`}
        onConfirm={() => (handleLink(item))}
      >{{
        reference: () => (
          <el-link
            class="mr-4"
            target="_blank"
            type={item.type || 'danger'}
            v-hasPermi={item.authKey}
            {...itemConf}
          >
            {item?.name}
          </el-link>
        )
      }}</el-popconfirm>)
      : (
        <el-link
          class="mr-4"
          target="_blank"
          type={item.type || 'primary'}
          v-hasPermi={item.authKey}
          onClick={() => (handleLink(item))}
          {...itemConf}
        >
          {item.name}
        </el-link>
      )
  })
};
// 表格单元格内容渲染
export const columnRender = (scope, item) => {
  const {
    render,
    valueType = PROTABLE_RENDER_TYPE.INPUT,
    props
  } = item;
  const renderType = isFunction(valueType) ? valueType(scope, item) : valueType;
  const configs = (isFunction(props) ? props(scope, item) : props) || {};
  // 获取当前列默认数据
  const rowText = scope.row[item.prop];
  // 自定义render
  const renderContent = isFunction(render) ? render(scope) : render;
  // render有效 ? render : rowText
  const renderRes = isTruthyOrZero(renderContent) ? renderContent : rowText;
  // 渲染类型的返回配置
  const renderTypes = ({
    // 默认
    ['textarea']: () => renderRes,
    [PROTABLE_RENDER_TYPE.INPUT]: () => renderRes,
    // HoyoFormatNumber 数字格式化
    [PROTABLE_RENDER_TYPE.NUMBER]: () => numberFormat(renderRes, configs.precision),
    // hoyoTag 状态
    [PROTABLE_RENDER_TYPE.SELECT]: () => {
      const valueEnum = configs.valueEnum || [];
      const currentVal = valueEnum.find(it => it.value === rowText);
      return currentVal && (<el-tag type={currentVal.elTagType}>{currentVal.label}</el-tag>);
    },
    // 操作列
    [PROTABLE_RENDER_TYPE.OPERATE]: () => getActionList(configs.actionList),
    [PROTABLE_RENDER_TYPE.DATE]: () => {
      const val = configs.value || rowText
      return val && dateFormat(val, configs.format || 'YYYY-MM-DD')
    },
  });
  const res = renderTypes[renderType]?.();
  if (!isTruthyOrZero(res)) return null;
  return res;
};
// table-column的header插槽
const headerSlot = (scope, item) => {
  const {header} = item;
  const label = isFunction(header) ? header(scope) : header;
  const labelText = label || item.label;
  return item.tip ? (
    <span>
      {labelText}
      <i class="mi-icon-question text-md ml-xs" v-tooltip={item.tip}/>
    </span>
  ) : labelText;
};

// table-column的default插槽
const defaultSlot = (scope, item) => {
  const text = columnRender(scope, item);
  // 超出3行并且有值...悬浮展示全部
  if (item.ellipsis && text) {
    return (
      <el-tooltip placement="top" effect="light" content={text}>
        <p class="overflow-ellipsis overflow-hidden line-clamp-3">{text}</p>
      </el-tooltip>
    );
  }
  // 没有值的时候返回占位符
  return isTruthyOrZero(text) ? text : '-';
};
const useTableCol = (columns) => {
  const column = computed(() => getColumns(columns.value))

  const tableColRender = () => {
    return column.value?.map((item) => {
      const slot = {
        ...item?.slot?.(),
        header: (scope) => headerSlot(scope, item),
        default: (scope) => defaultSlot(scope, item),
      }
      return (
        <el-table-column
          {...item}
        >
          {slot}
        </el-table-column>
      )
    })
  }
  return {
    tableColRender
  }
}
export default useTableCol
