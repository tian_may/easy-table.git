import {ref} from 'vue'

const defaultColConf = {
  width: '70',
  fixed: 'left',
  align: 'center',
  type: 'selection',
}
const useRowSelector = (rowSelection) => {
  const rowSelectParam = ref('')
  const config = {...defaultColConf, ...rowSelection.value}
  const rowSelectionRender = () => {
    return rowSelection.value &&
      (rowSelection.value?.type === 'radio' ? (
        <el-table-column
          {...config}
          width="70"
          align="center"
        >
          {{
            default: ({row}) => (
              <el-radio
                class="hideLabel"
                onChange={rowSelection.value.onSelectionChange}
                v-model={rowSelectParam.value}
                label={row?.[rowSelection.value?.rowKey]}
              />
            )
          }}
        </el-table-column>
      ) : (
        <el-table-column
          {...config}
        />
      ))
  }

  return {
    rowSelectionRender,
  }
}
export default useRowSelector
