import {computed} from 'vue'

// 单元格合并
function splitArr2Arr(list, key) {
  if (!list.length) return []
  if (!key) return list
  let mySet = new Set()
  list.forEach((item) => {
    mySet.add(item[key])
  })
  return groupArr2Arr(list, [...mySet], key)
}

/**groupArr2Arr将list按照group中的key值进行分组
 * @param {Array} list 要分割的数组
 * @param {Object} group 要分割的规则数组
 * @param {Object} key 字段名
 */
function groupArr2Arr(list, group, key) {
  let obj = {}
  group.forEach((i) => {
    obj[i] = []
    list.forEach((item) => {
      if (item[key] === i) {
        obj[i].push(item)
      }
    })
  })
  let arr = []
  Object.keys(obj).forEach((item) => {
    arr.push({
      [key]: item,
      list: obj[item]
    })
  })
  return arr
}


function getMerge(mergeCol, data) {
  if (!data) return []
  const res = data
  mergeCol?.map?.((obj) => {
    const groups = splitArr2Arr(data, obj.prop)
    const res = []
    groups?.forEach?.((arr) => {
      arr?.list?.forEach?.((item, index) => {
        item[`rowSpan_${obj.prop}`] = index === 0 ? arr?.list?.length : 0
      })
      res?.concat?.(arr?.list)
    })
  })
  return res || []
}

const getMergeCol = (columns, rowSelection) => {
  return columns?.filter?.((el, index) => {
    if (el.merge === 'col') {
      return el
    }
    //当表格为可选表格时 chekcbox/radio所在列展示 合计
    if (index === 1 && columns[1].merge === 'col' && rowSelection) {
      return el
    }
  })
}

// 单元格合并
export const objectSpanMethodFun = ({row, column}, mergeCol) => {
  const props = mergeCol?.map((el) => el.prop)
  if (props.includes(column.property)) {
    const rowspan = row[`rowSpan_${column.property}`]
    const res = [rowspan, rowspan > 0 ? 1 : 0]
    return res
  }
}

const useMerge = ({columns, rowSelection, defaultSelection, dataList}) => {

  // 可合并列
  const mergeCol = getMergeCol(columns.value, rowSelection.value)
  const dataSource = computed(() => getMerge(mergeCol, dataList.value))
  //表格选择
  return {
    spanMethod: ({row, column}) => objectSpanMethodFun({row, column}, mergeCol),
    dataSource,
  }
}
export default useMerge
