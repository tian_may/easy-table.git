import SearchForm from "@/components/XTable/components/ProForm.vue";
import {Search, Refresh} from '@element-plus/icons-vue'
import {getSearchConfig} from "@/components/XTable/helper.jsx";

export const formFormat = (formData, formConfig) => {
  return Object.entries(formData).reduce((obj, [key, val]) => {
    const item = formConfig.find((it) => it.prop === key);
    const {fetchFormat} = item || {};
    let value = val;
    if (isFunction(fetchFormat)) {
      value = fetchFormat(val);
    }
    if (!(!value || (Array.isArray(value) && !value?.length))) {
      obj[key] = value;
    }
    return obj;
  }, {});
};

const useSearchForm = (props) => {
  const searchConfig = computed(() => getSearchConfig(props?.searchConfig?.value || props?.columns?.value))
  const searchRender = () => {
    return (
      <SearchForm
        {...props}
        columns={searchConfig.value}
        value={props.params?.value}
        footer={{
          reset: {
            text: '重置',
            icon: Refresh
          },
          confirm: {
            text: '查询',
            icon: Search
          }
        }}
      />
    )
  }
  return {
    searchRender,
  }
}
export default useSearchForm
