import { createRouter, createWebHistory } from 'vue-router'

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: "/editTable/demo",
      name: "editTableDemo",
      component: () =>
          import("../views/demo/editTable/template.vue"),
      meta: {
        title: "可编辑表格demo",
        showLink: false,
        rank: 101
      }
    },
    {
      path: "/editTable/demo_jsx",
      name: "editTableDemoJsx",
      component: () => import("../views/demo/editTable/jsx.vue"),
      meta: {
        title: "可编辑表格demo_jsx",
        showLink: false,
        rank: 101
      }
    },
    {
      path: "/",
      name: "SingleRowEditDemo",
      component: () =>
          import("../views/demo/editTable/singleRowEdit.vue"),
      meta: {
        title: "单行—可编辑表格demo_jsx",
        showLink: false,
        rank: 101
      }
    },
    {
      path: "/proTable/demo",
      name: "proTableDemo",
      component: () =>
          import("../views/demo/proTable/template.vue"),
      meta: {
        title: "复合表格demo",
        showLink: false,
        rank: 101
      }
    },
    {
      path: "/proTable/demo_jsx",
      name: "复合表格demo_jsx",
      component: () => import("../views/demo/proTable/jsx.vue"),
      meta: {
        title: "demo",
        showLink: false,
        rank: 101
      }
    },
    {
      path: "/searchForm/demo",
      name: "searchFormDemo",
      component: () =>
          import("../views/demo/searchForm/template.vue"),
      meta: {
        title: "搜索表单demo",
        showLink: false,
        rank: 101
      }
    },
    {
      path: "/searchForm/demo_jsx",
      name: "searchFormDemoJsx",
      component: () => import("../views/demo/searchForm/jsx.vue"),
      meta: {
        title: "搜索表单demo_jsx",
        showLink: false,
        rank: 101
      }
    },
  ]
})

export default router
