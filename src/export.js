import ProTable from './components/EasyTable/ProTable.vue';
import EditTable from './components/EasyTable/EditTable.vue';

export { ProTable, EditTable}

const components = [EditTable, ProTable]

const INSTALLED_KEY = Symbol('INSTALLED_KEY')
export const install = (app) => {
    if (app[INSTALLED_KEY]) return
    app[INSTALLED_KEY] = true
    components.forEach(comp => app.use(comp))
}

export default {install}
