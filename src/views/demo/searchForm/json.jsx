export const columns = [
  { label: "姓名", prop: "name" },
  { label: "曾用名", prop: "alias" },
  {
    label: "性别",
    prop: "gender",
    valueType: "select",
    options: [
      { label: "男", value: 1 },
      { label: "女", value: 2 }
    ]
  },
  {
    label: "年龄",
    prop: "age",
    valueType: "number",
    max: 150,
    min: 1,
    precision: 0
  },
  {
    label: "出生日期",
    prop: "date",
    valueType: "date"
  }
];
